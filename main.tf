

### Instance EC2 
###      -----> SSH-PUBLIC-KEV
###      -----> SG (Security Group)  [22/TCP]

resource "aws_key_pair" "mySSH-key" {

    key_name = var.ssh_key_name
    public_key = var.public_ssh_key
}

resource "aws_security_group" "mySG" {
   
    description = "Security group to allow incoming SSH connection to ec2 instance"
    name = "mySG"

    ingress = [ {
        cidr_blocks = [ "0.0.0.0/0" ]
        description = "allow SSH"
        from_port = 22
        ipv6_cidr_blocks = []
        prefix_list_ids = []
        protocol = "TCP"
        security_groups = []
        self = false 
        to_port = 22
    }]

    egress = [ {
        description = "allow connection to any internet service"
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        self = false
        ipv6_cidr_blocks = []
        prefix_list_ids = []
        security_groups = []
        
 
            }]
}
resource "aws_instance" "myec2" {

    ami = "ami-06878d265978313ca"
    instance_type = "t2.medium"
    key_name = aws_key_pair.mySSH-key.key_name  #1ère variable terraform
    security_groups = [aws_security_group.mySG.name]
    tags = {

        "Name" = "maVM"
    }
}


