variable "aws_region" {

    descirption = "Region of aws deployment"
    type = string
    default = "us-east-1"
}

variable "aws_access_key"{

    description = "aws access key for account"
    type = string 
}

variable "aws_secret_key"{

    description = "aws secret key for account"
    type = string 
}

variable "aws_token_session"{

    description = "aws session token"
    type = string 
}

variable "ssh_key_name" {

    description = "Name of SSH key"
    type = string 
}

variable "public_ssh_key" {

    description = "public SSH key to add on aws account"
    type = string 
}


